//  Author: George Strauch
//  serial number: 54
//  Due Date: March 27 2019
//  Programming Assignment: 4
//  Spring 2019 - CS 3358 - 252
//  Instructor: Husain	Gholoom
//
// singly linked list using pointers


#include <iostream>
#include <cmath>
using namespace std;


// this is the actual node object
// value: the value that the node has and can be used to prove the node exists
// next*: pointer to the next node in the list (will be Null for the tail node)
struct Node
{
    int value;
    Node* next;
};


// container for the linked list
// contains functionality to do things to the list
// head: the first element in hte list
class SinglyLinkedList
{
public:
    SinglyLinkedList();
    void buildList(); // A
    SinglyLinkedList appendAnotherList(SinglyLinkedList toAppend);  // B
    SinglyLinkedList removeDuplicates(); // C
    void insert(int location, int value); // D
    void displayList(); // A, B, C, D, F
    void getNumElements(); // E
    void removeElements(int index); // F
    void displayListBackwards(); // G
    Node* getHead();
    Node* getTail();
    SinglyLinkedList clone();
    void addIfValNotExist(int val);

private:
    void setList (Node *newHead);
    Node* head;
    // it is not necessary to have tail field because
    // it is easier to find the address of the tail node when specifically needed
    // rather than to re-find the tail every time it might change
    //
    // when iterating through a list, while (tmp != this->tail) is the same as saying while (tmp->next != NULL)
};


// constructor will initialize the head node and set the rest of the linked list
SinglyLinkedList::SinglyLinkedList ()
{
    this->head = new Node();
    this->buildList();
}


// this will append another list to the list that calls this function and returns that new list
// this function does not change the list object that calls it or the list that is being appended
// this needs to make an entire new list with different node objects other than the predefined lists,
// meaning the new list cannot just point to the head of this list because the tail of this would
// then point to the start of the next list
// param 1: list that will be appended to the current list
SinglyLinkedList SinglyLinkedList::appendAnotherList (SinglyLinkedList toAppend)
{
    SinglyLinkedList copyOfThis = this->clone();
    SinglyLinkedList copyOfToAppend = toAppend.clone();

    // sets the next of the tail of the first to the head of the second
    copyOfThis.getTail()->next = copyOfToAppend.getHead();

    // by now copyOfThis List is the 2 lists appended together
    return copyOfThis;
}


// makes copy of this SinglyLinkedList object and returns it
SinglyLinkedList SinglyLinkedList::clone ()
{
    // declares a new mem address for a new node
    Node *headOfNewList = new Node();

    // the value of the the headOfNewList node is the same as the head of this but in a different mem address
    *headOfNewList = *this->head;

    // needs to keep head of new list so headOfNewList will not change
    // create a new node (tmp) to iterate
    Node *tmp = headOfNewList;

    while(tmp->next != NULL)
    {
        // creates new node and sets it to the same value as tmp->next
        Node newNext = *tmp->next;
        tmp = &newNext;
    }

    SinglyLinkedList toReturn = SinglyLinkedList();
    toReturn.setList(headOfNewList);
    return toReturn;
}


// sets the list
// by replacing the head, the whole list is reset
// Param 1: pointer to new head node
void SinglyLinkedList::setList (Node *newHead)
{
    this->head = newHead;
}


// returns the tail
// iterates through the list until the next node is null then returns that node address
Node* SinglyLinkedList::getTail ()
{
    Node* tmp = this->head;
    while(tmp->next != NULL)
    {
        tmp = tmp->next;
    }
    return tmp;
}


// returns pointer to the head node
Node* SinglyLinkedList::getHead ()
{
    return this->head;
}


// builds the LL containing 15 nodes
// each having a value between 0 and 25 inclusive
void SinglyLinkedList::buildList ()
{
    int listLength = 15;
    Node *tmp = this->head;

    // set the value of head first because tmp = tmp->next is the first operation in the loop
    // otherwise the tail will remain uninitialized
    tmp->value = rand() % 26;

    for (int i = 1; i < listLength; ++i)
    {
        tmp->next = new Node();
        tmp = tmp->next;
        tmp->value = rand() % 26;
    }
}


// helper function to remove duplicates
// this function will only be called by the tmp list in removeDuplicates ()
// param 1: the value of the node that will be added to the list if there is not yet a node with that value
void SinglyLinkedList::addIfValNotExist(int val)
{
    Node* tmp = this->head;

    while(tmp != NULL)
    {
        // cancels adding node to list if val already exists
        if (tmp->value == val)
        {
            return;
        }
        tmp = tmp->next;
    }

    // if reached here, value does not exist in list
    Node *toAdd = new Node ();

    toAdd->value = val;
    toAdd->next = NULL;

    if (head == NULL)
    {
        head = toAdd;
        return;
    }

    this->getTail()->next = toAdd;
}


// removes duplicates form a list
// does not change the current object, just returns a new SinglyLinkedList obj
// with the same list but with duplicates removed
// returns a SinglyLinkedList with out any repeating node values
SinglyLinkedList SinglyLinkedList::removeDuplicates ()
{
    SinglyLinkedList listWOutRepeats;
    listWOutRepeats.setList(NULL);

    // node to iterate
    Node *tmp = this->head;

    while (tmp != NULL)
    {
        listWOutRepeats.addIfValNotExist(tmp->value);
        tmp = tmp->next;
    }

    return listWOutRepeats;
}


// inserting at location 0 means making the new node the head
// and the old head is the second element
// inserting at location -1 means make the new node the tail
// param 1: index of the list where the new node is being inserted at
// param 2: the value of that node
void SinglyLinkedList::insert(int location, int value)
{
    Node *toInsert = new Node();
    toInsert->value = value;
    Node *tmp = this->head;

    // insert at end
    if(location == -1)
    {
        while(tmp->next != NULL)
        {
            tmp = tmp->next;
        }
        toInsert->next = NULL;

        // now tmp-> next is null, set this to be a new node with the given value
        tmp->next = toInsert;
        return;
    }

    else if (location == 0)
    {
        this->head = toInsert;
        this->head->next = tmp;
    }

    else
    {
        for (int i = 0; i < location-1; ++i) {
            if (tmp->next == NULL)
            {
                cout << "Trying to access nonexistent Node... Aborting insert of new node\n";
                return;
            }
            tmp = tmp->next;
        }
        // works if tmp-> next is null
        Node *restOfList = tmp->next;
        tmp->next = toInsert;
        toInsert->next = restOfList;
    }
}


// prints out values for each element in the list
// only prints max of 15 elements per line
void SinglyLinkedList::displayList ()
{
    int nodeNum = 0;
    Node* tmp = this->head;

    while (tmp != NULL)
    {
        if (nodeNum % 15 == 0) { cout << '\n'; }
        cout << tmp->value << " ";
        nodeNum++;
        tmp = tmp->next;
    }
    cout << "\n";
}



// prints the number of elements
// iterates though the list with counter
void SinglyLinkedList::getNumElements ()
{
    int nodeNum = 0;
    Node* tmp = this->head;

    while (tmp != 0)
    {
        tmp = tmp->next;
        nodeNum++;
    }
    cout << nodeNum << '\n';
}


// removes the node of the given element
// index 0 means remove head and set head->next as head
// index -1 removes tail
// param 1: index of node to remove
void SinglyLinkedList::removeElements (int index)
{
    if(index == 0)
    {
        this->head = this->head->next;
    }
    else
    {
        Node* tmp = this->head;

        // remove tail the return
        if(index == -1)
        {
            // while next node is not the tail
            while(tmp->next->next != NULL)
            {
                tmp = tmp->next;
            }
            tmp->next = NULL;
            return;
        }

        // remove indexed node and return
        for (int i = 0; i < index-1; ++i) {
            if (tmp->next == NULL)
            {
                cout << "Trying to access nonexistent Node... Aborting removal of node\n";
                return;
            }
        }

        // removing tmp->next so need to test if that is null, other wise will not work
        if (tmp->next == NULL)
        {
            cout << "Trying to access nonexistent Node... Aborting removal of node\n";
            return;
        }
        tmp->next = tmp->next->next;
    }
}


// displays the list backwards
// works by making a disposable list similar to this
// iterates through disposable list, pops the head off each time and sets it as the new head to the new list
void SinglyLinkedList::displayListBackwards ()
{
    // makes a SinglyLinkedList Obj = to a copy of this so that the actual list is not affected
    SinglyLinkedList disposableCloneOfThis = this->clone();
    SinglyLinkedList backwardsList;

    // make head of new List = NULL
    backwardsList.setList(NULL);

    // value of current node in list
    int tmpVal = 0;

    // iterates through the disposable clone
    // sets the head of the backwards list = to a copy of the head of disposableCloneOfThis
    // every iteration deletes the head of disposableCloneOfThis until nothing of the list remains
    while (disposableCloneOfThis.getHead() != NULL)
    {
        // sets tmpVal
        tmpVal = disposableCloneOfThis.getHead()->value;

        // insert a new node on the backwards list as the head with
        backwardsList.insert(0, tmpVal);

        // remove the head of the disposable clone
        disposableCloneOfThis.removeElements(0);
    }

    // displays list that is now = to disposableCloneOfThis in reverse order
    backwardsList.displayList();
}


// implementation
int main ()
{
    cout << "Welcome to my Singly LL Program\n";

    // A
    cout << "\nA - build list_1 and List_2, each consisting of 15 elements"
            " with values 0-25 inclusive, then display them\n";
    SinglyLinkedList list_1;
    SinglyLinkedList list_2;
    cout << "list_1:";
    list_1.displayList();
    cout << "list_2:";
    list_2.displayList();

    // B
    cout << "\n\nB - create list_3 by appending list_2 to list_1, then display list_3\n";
    SinglyLinkedList list_3 = list_1.appendAnotherList(list_2);
    cout << "list_3:";
    list_3.displayList();

    // C
    cout << "\n\nC - create list_4 by removing duplicates from list_3, then display list_4\n";
    SinglyLinkedList list_4 = list_3.removeDuplicates();
    cout << "list_4:";
    list_4.displayList();

    // D
    cout << "\n\nD - insert 100, 200 in the first 2 locations of list_4,"
            " and 88, 99 in the last locations, then display list_4\n";
    list_4.insert(0, 200);
    list_4.insert(0, 100);
    list_4.insert(-1, 88);
    list_4.insert(-1, 99);
    cout << "list_4:";
    list_4.displayList();

    // E
    cout << "\n\nE - display number of elements of list_4\n";
    cout << "number of elements in list_4: ";
    list_4.getNumElements();

    // F
    cout << "\n\nF - delete the first and last elements of list_4, then display list_4\n";
    list_4.removeElements(0);
    list_4.removeElements(-1);
    cout << "list_4:";
    list_4.displayList();

    // G
    cout << "\n\nG - display list_4 backwards\n";
    cout << "list_4 backwards: ";
    list_4.displayListBackwards();

    cout << "\nThis Simple LL Program is implemented by:\nGeorge Strauch - March 27th, 2019\n";

    return 0;
}